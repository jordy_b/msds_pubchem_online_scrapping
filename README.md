# msds_PubChem_online_scrapping

National Center for Biotechnology Information is a place where you can find several informations regarding chemical and safety.  
One part of NCBI is PubChem (https://pubchem.ncbi.nlm.nih.gov/)

The demand was to add GHS phrases (safety advices) to an excel file. And later to add flash point (a specific property of the compound).  
Excel file processed was an extraction from HSPiP. We had acess to rnCAS and InchiKey of each compound, wich allowed us to webscrap the whole excel file.  

## Files description:  
**solvant_webscrap.py** is the code that websacrappes pubchem MSDS sheets save them to a folder and write GHS phrases found  
Main idea here is to use these 2 urls:  
    - https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pccompound&term={rnCas}  
    Where you want to replace rnCAS by the rnCAS number of the chemical  
    Get the PubChem ID of the compound and use it here:  
    - https://pubchem.ncbi.nlm.nih.gov/rest/pug_view/data/compound/{UID}/JSON  
    Then you can search for GHS phrases  

**json_manipulation.py** is the code that run through all the MSDS sheets (as json files) to gather Flash Point parameter if present.  
Main idea here is to load every json file, and run through every threads and sub-threads to find the flashpoint.  
You can easily extend the idea to get other informations in the MSDS.  

**Base HSPIP 5.0.04 10K.xlsx** initial Ecxel file  

**HSPIP_5.0.04_10K_GHS_FPpc.xlsx** final result  
  
  
modules needed are json, requests and pandas