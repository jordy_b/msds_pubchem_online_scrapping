''' Webscrapping of MSDS sheets on online PubChem
https://www.ncbi.nlm.nih.gov/
solvant_webscrap use ncbi.nlm.nih.gov MSDS dataset to gather
GHS phrases and some phisical informations on top of a HSPiP dataset (10k + rows).

NCBI - Entrez Programming Utilities : WEB SERVICE, BY URL SEARCH SEE:
https://www.ncbi.nlm.nih.gov/books/NBK25500/#chapter1.Downloading_Document_Summaries

retrieve GHS classification for all chemicals in "Base HSPIP 5.0.04 10K.xlsx"
'''

import os
import re
import time
import json
import requests
import numpy as np
import pandas as pd

# API key given by www.ncbi.nlm.nih.gov
API_key = 'jordy.bonnet@solvay.com'
# folder where msds sheets are going to be gathered
json_msds_folder = r'C:\Users\jbonnet\Documents\Python\etudes\coatis_hspip_ghs\msds'

# HSPiP excel file that will be processed
excel_file_path = r'C:\Users\jbonnet\Documents\Python\etudes\coatis_hspip_ghs\HSPIP_5.0.04_10K_FP.xlsx'
df = pd.read_excel(excel_file_path)

# new excel file that will be created
new_excel = r'C:\Users\jbonnet\Documents\Python\etudes\coatis_hspip_ghs\HSPIP_5.0.04_10K_GHS_FP.xlsx'
writer = pd.ExcelWriter(new_excel)

# list of GHS phrases
GHS_H_list = ['H200','H201','H202','H203','H204','H205','H206','H207','H207','H208','H220','H221','H222','H223','H224','H225','H226','H227','H228','H228','H229','H229','H229','H230','H231','H232','H240','H241','H242','H242','H250','H251','H252','H260','H261','H261','H270','H271','H272','H272','H280','H281','H290','H300','H301','H302','H303','H304','H305','H310','H311','H312','H313','H314','H315','H316','H317','H318','H319','H320','H330','H331','H332','H333','H334','H335','H336','H340','H341','H350','H350i','H351','H360','H360F','H360D','H360FD','H360Fd','H360Df','H361','H361f','H361d','H361fd','H362','H370','H371','H372','H373','H400','H401','H402','H410','H411','H412','H413','H420']
df['H_all'] = ''
for H in GHS_H_list:
    df[H] = 0
df['pubchemStatus'] = ''

# df.iterrows()     df.sample(20, random_state=1337).iterrows()   df[:20].iterrows()      df.iloc[10228:10228+1].iterrows()
for row in df.iterrows():
    prod = row[1]['Name']
    rnCas = str(row[1]['CAS'])
    InChiKey = row[1]['InChiKey']

    # get CAS number from excel file
    rnCas_g = re.findall('\d+-\d\d-\d', str(rnCas))
    if rnCas_g == []:
        id_state = 'cas not good'
        df.at[row[0], 'pubchemStatus'] = id_state
        print(row[0], rnCas, id_state)
        continue
    else:
        rnCas = rnCas_g[0]

    # 1 - get pubchem ID with rnCas (or InchiKey)
    url_rncas = f'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pccompound&term={rnCas}&email={API_key}'
    r = requests.get(url_rncas)
    time.sleep(0.3)
    # get IDs
    UID = re.findall('<Id>\d+', r.text)

    id_state = 'found'

    if (len(UID) > 1):
        # if more than one ID found in pubchem with rnCAS, try rnCAS + InChikey
        url = f'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pccompound&term={rnCas}+{InChiKey}&email={API_key}'
        r = requests.get(url)
        time.sleep(0.3)
        UID = re.findall('<Id>\d+', r.text)
        if (len(UID) > 1):
            # if still more than one ID - do not process it ! too suspicious
            id_state = 'suspicious - more than one ID'
            df.at[row[0], 'pubchemStatus'] = id_state
            print(row[0], rnCas, InChiKey, UID, id_state)
            continue
        if (len(UID) == 0):
            # if nothing found with the rnCAS/InchiKey combinaison - do not process it
            id_state = 'Not found'
            df.at[row[0], 'pubchemStatus'] = id_state
            print(row[0], rnCas, InChiKey, id_state)
            continue
    elif (len(UID) == 0):
        # if nothing found with rnCAS try with inchikey only
        url = f'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pccompound&term={InChiKey}&email={API_key}'
        r = requests.get(url)
        time.sleep(0.3)
        UID = re.findall('<Id>\d+', r.text)
        if (len(UID) > 1):
            # if still more than one ID - do not process it ! too suspicious
            id_state = 'suspicious - more than one ID'
            df.at[row[0], 'pubchemStatus'] = id_state
            print(row[0], rnCas, InChiKey, UID, id_state)
            continue
        if (len(UID) == 0):
            # nothing found either with rnCAS or InchiKey
            id_state = 'Not found'
            df.at[row[0], 'pubchemStatus'] = id_state
            print(row[0], rnCas, InChiKey, id_state)
            continue

    UID = UID[0][4:]        # ID - from list to str

    print(row[0], rnCas, UID, id_state)

    # 2 - get MSDS json file
    url2 = f'https://pubchem.ncbi.nlm.nih.gov/rest/pug_view/data/compound/{UID}/JSON'
    r = requests.get(url2)
    time.sleep(0.3)

    msds = r.text
    json_msds = json.loads(msds)

    # save json_msds into specific folder
    outfile = os.path.join(json_msds_folder, f'{rnCas}_{UID}.json')
    with open(outfile, 'w') as file:
        json.dump(json_msds, file)

    # 3 - find GHS phrases
    filter_H = r'\bH\d\d\d\b'
    rech = re.findall(filter_H, msds)

    Hs = np.unique(rech)
    print(f'{prod} -> {np.unique(rech)}')
    df.at[row[0], 'pubchemStatus'] = id_state
    df.at[row[0], 'H_all'] = str(Hs)[1:-1].replace("'", "")
    for H in Hs:
        if H in GHS_H_list:
            df.at[row[0], H] = 1

    if np.mod(row[0], 200) == 0:
        if row[0] != 0:
            print(f'{row[0]} / {df.shape[0]} - {row[0] / df.shape[0] * 100:.2f}%')
            df.to_excel(writer, 'HSPIP')
            writer.save()

print('End')





''' PART OF CODEFOR PHYSICAL PARAMETERS

def find_physical(phys_name='Boiling Point'):
    # rep = re.search(f'"Name": "{phys_name}",', json_msds_txt)
    out = []
    for match in re.finditer(f'"Name": "{phys_name}",', json_msds_txt):
        print('\n', match.span(), match.group())
        Val_m = re.search(f'"StringValue": "[^"]*"', json_msds_txt[match.span()[1]:])
        print(Val_m.span(), Val_m.group()[16:-1])
        out.append(Val_m.group()[16:-1])

# parameters available
boil = find_physical('Boiling Point')
    melt = find_physical('Melting Point')
    flash = find_physical('Flash Point')
    pH = find_physical('pH')
    'Physical Description'
    'Color'
    'Odor'
    'Flash Point'
    'Solubility' # <-- à retravailler
    'Density'
    'Vapor Density'
    'Vapor Pressure'
    'LogP'
    'LogS'
    'Stability'
    'Auto-Ignition'
    'Decomposition'
    'Viscosity'
    'Corrosivity'
    'Heat of Combustion'
    'Heat of Vaporization'
    'pH'
    'Surface Tension'
    'Ionization Potential'
    'Polymerization'
    'pKa'
    'Dissociation Constants'
    'Relative Evaporation Rate'
'''


''' TRY TO FIND Hxxx IN THE JSON
json_msds = json.loads(json_msds_txt)
    # save json_msds
    with open('data.json', 'w') as outfile:
        json.dump(json_msds, outfile)

    phrases = json_msds['Record']['Section'][13]['Section'][0]['Section'][0]['Information'][0]['StringValue']

'''