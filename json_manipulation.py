'''
db analysis with pandas
'''

import os
import re
import time
import json
import numpy as np
import pandas as pd

# folder where msds sheets have been gathered
json_msds_folder = r'C:\Users\jbonnet\Documents\Python\etudes\coatis_hspip_ghs\msds'
files_names = os.listdir(json_msds_folder)

# excel file that will be processed
excel_file_path = r'C:\Users\jbonnet\Documents\Python\etudes\coatis_hspip_ghs\HSPIP_5.0.04_10K_GHS_FP.xlsx'
df = pd.read_excel(excel_file_path)

# new excel file that will be created
new_excel = r'C:\Users\jbonnet\Documents\Python\etudes\coatis_hspip_ghs\HSPIP_5.0.04_10K_GHS_FPpc.xlsx'
writer = pd.ExcelWriter(new_excel)

# add new columns for new excel file
df['min Flash Point (°C)'] = ''
df['max Flash Point (°C)'] = ''
df['pubchem FP status'] = ''

def minmaxFlashPoint(json_msds):
    ''' function that run through the json chapters / threads to get flashpoint property of the product '''
    FP = []
    etat = 'chapter Chemical and Physical Properties not found'
    for i0, chapter in enumerate(json_msds['Record']['Section']):
        if chapter['TOCHeading'] == 'Chemical and Physical Properties':
            etat = 'Thread Experimental Properties not found'
            for i1, ExpProp in enumerate(chapter['Section']):
                if ExpProp['TOCHeading'] == 'Experimental Properties':
                    etat = 'Property Flash Point not found'
                    for i2, PhisProp in enumerate(ExpProp['Section']):
                        if PhisProp['TOCHeading'] == 'Flash Point':
                            for info in PhisProp['Information']:
                                if 'StringWithMarkup' in info['Value']:
                                    val = info['Value']['StringWithMarkup'][0]['String']
                                    # print(f'\n{val}')
                                    # a = re.search('\d+\s*[°degDEG]+\s*[FfcC]*', val)
                                    a = re.search('[-]*\d+[.]*\d*\s*[°degDEG]+\s*[FfcC]', val)
                                    if a != None:
                                        a = a.group(0)
                                        # print(a)
                                        T_unit = a[-1]
                                        T_val = int(re.search('[-]*\d+', a).group())
                                        if (T_unit == 'F') or (T_unit == 'f'):
                                            num_C = (T_val - 32) * 5 / 9
                                        if (T_unit == 'C') or (T_unit == 'c'):
                                            num_C = T_val
                                        FP.append(int(num_C))
                                    # print(f'min: {min(T_C)} °C, max: {max(T_C)} °C')
                            etat = 'Property Flash Point found'
    if FP == []:
        FP = [np.NAN, np.NAN]
    return min(FP), max(FP), etat

# go through every row of the excel file - df.iterrows()     df.iloc[10228:10228+1].iterrows()
for row in df.iterrows():
    rnCas = str(row[1]['CAS'])
    # find the msds sheet file name in all the folder
    for file_name in files_names:
        if re.split('_', file_name)[0] == rnCas:
            # when file found...
            file_path = os.path.join(json_msds_folder, file_name)
            # load it...
            with open(file_path, 'r') as read_file:
                json_msds = json.load(read_file)
            # use main funciton to retrieve Flash point
            min_T, max_T, etat = minmaxFlashPoint(json_msds)
            print(f'{row[0]}. {file_name} - [{min_T}, {max_T}] - {etat}')
            df.at[row[0], 'min Flash Point (°C)'] = min_T
            df.at[row[0], 'max Flash Point (°C)'] = max_T
            df.at[row[0], 'pubchem FP status'] = etat

    if np.mod(row[0], 200) == 0:
        if row[0] != 0:
            print(f'\n{row[0]} / {df.shape[0]} - {row[0] / df.shape[0] * 100:.2f}%')

# add it to the new excel file
df.to_excel(writer, 'HSPIP')
writer.save()


